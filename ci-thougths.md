# CI - Was wollen wir eigentlich?

## Vorraussetzungen

  * Wir bauen Maven Projekte mit Abhängikeiten zu anderen Maven Projekten die 
  unter unser Kontrolle stehen
  * Versionen von Abhänglikeiten sind in unseren `pom.xml` Datein über Properties
  verwaltet
  
## Ziele
  
  * Ein Push und man sieht im Ergebnis ob dieses Änderung in anderen Projekten
  etwas kaputt macht
  * Möglichst geringe Abhängikeiten zu anderen Produkten, wie z.B. gitlab, nexus, etc
  
## Lösungen

### Gitlab-Ci, Nexus
  
  1. Es wird für jeden Push ein Nexus Repo angelgt
  1. In jedem `.gitlab-ci.yml` wird hinterlegt welche Projekte von dem aktuellen
  abhängig sind
  1. Es git ein maven Profil in dem Hinterlegt ist, das dass intermins Nexus Repo
  genutzt wird

#### Vorteile
  * Es funktioniert
  * Mehrere Build-Nodes nutzbar

#### Nachteile
  * Abhängig von gitlab-ci (kann als SAS genutzt werden)
  * Abhängig von eigener Nexus Instanz
  * Redundante Konfiguration: In jedem Projekt wird in `.gitlab-ci.yml` hinterlegt
    welche Projekte von dem Projekt abhängig sind, in `pom.xml` wird hinterlegt
    von welechen Projekten ein Projekt abhängig ist