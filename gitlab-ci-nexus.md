## [rapidpm-dependencies](https://gitlab.com/RapidPM/rapidpm-dependencies)
### Build
  * `mvn deploy`
  * pittest
  
### Trigger

  * dynamic-cdi
  * Functional Reactive
  * vpiwiktracker
  
## [dynamic-cdi](https://gitlab.com/RapidPM/dynamic-cdi)

TODO: deploy schlägt fehl weil `org.rapidpm.dynamic-cdi:rapidpm-dynamic-cdi-modules-core:jar:sources:1.0.2-20180108.192259-1` mehrmals hochgeladen werden soll

### Build
  * `mvn deploy`
  
### Trigger
  * rapidpm-microservice

## [functional-reactive-lib](https://gitlab.com/Functional-Reactive/functional-reactive-lib)
### Build

  * `mvn deploy`
  * pittest

### Trigger
  
  * vaadin-addons
  
   
## [rapidpm-microservice](https://gitlab.com/RapidPM/rapidpm-microservice)
  * TODO Version aktuallisieren